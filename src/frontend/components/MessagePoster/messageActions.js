export const ADD_MESSAGE = 'ADD_MESSAGE';

export const sendMessage = (payload) => ({
    type: ADD_MESSAGE,
    payload
})
